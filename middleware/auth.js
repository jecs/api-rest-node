'use strict'

const services = require('../util/serviceToken');

function isAuth(req, res, next) {
    try {
        if (!req.headers.authorization) {
            return res.status(403).send({ status: 403, message: 'Forbidden'});
        }

        if( req.headers.authorization.split(" ")[0] != "Bearer"){
            return res.status(403).send({ status: 403, message: 'Forbidden'});
        }

        const token = req.headers.authorization.split(" ")[1];
                
        services.decodeSessionToken(token, mac)
        .then(response => {
            req.user = response;
            next();
        })
        .catch(err => {
            return res.status(err.status).send({message: err.message})
        })
    } catch(err) {
        return res.status(403).json({message: 'Forbidden'});
    }
}
module.exports = {
    isAuthLogin
}

