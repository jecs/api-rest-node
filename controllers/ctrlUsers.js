'user strict'

const serviceToken = require('../util/serviceToken');
const bcrypt = require('bcryptjs');
const { Accounts, Users, Session } = require('../db');

async function register (req, res) {    
    try {
        const user = {
            telefono: bcrypt.hashSync(req.body.telefono, 2)
        };
        
        req.body.password = bcrypt.hashSync(req.body.password, 9);
        
        let idUser = bcrypt.hashSync(req.body.telefono, 2);
        
        const new_account = {
            idusuario: idUser,
            telefono: req.body.telefono,
            email: req.body.email,
            password: req.body.password,
            user_token: serviceToken.createToken(user)
        };

        const new_user = {
            idusuario: idUser,
            nombre: req.body.nombre.toUpperCase(),
            ap_paterno: req.body.paterno.toUpperCase(),
            ap_materno: req.body.materno.toUpperCase(),
            fecha_nac: req.body.fechanac
        }

        userCreation(new_account, new_user)
        .then(response => {
            res.status(response.status).json(response)
        })
        .catch( err => {
            res.status(err.status).json(err)
        });
    } catch(err){
        res.status(500).json({
            status: 500,
            message: err.message
        })
    }
}

function userCreation(new_account, new_user) {
    return new Promise((resolve, reject) => {    
        try {
            Accounts.create(new_account)
            .then(resAccount =>{
                Users.create(new_user)
                .then(resUsers => {
                    resolve({
                        status: 201,
                        message: "Usuario creado",
                        data:{
                            token: new_account.user_token
                            }
                    })
                })
                .catch(err2 => {
                    reject({
                        status: 500,
                        message: err2.errors[0].type
                    });
                });
            })
            .catch(err => {
                reject({
                    status: 500,
                    message: err.errors[0].type
                });
            });
        } catch (err) {
            reject({
                status: 500,
                message: err.errors[0].type
            });
        }
});
}

async function getAllUsers (req, res) {    
    try {
        const data_user = await Users.findAll({});
        if (data_user){
            res.status(200).json({
                status:200,
                message:"Listado de usuarios...",
                data: data_user
                    
            });
        } else {
            res.status(404).json({
                status:404,
                message:"No existen usuarios registrados"
            });
        }
    } catch(err){
        res.status(500).json({
            status: 500,
            message: err.message
        })
    }
}

module.exports = {
    register,
    getAllUsers
}