const moment = require("moment");

module.exports = (sequelize, type) => {
    return sequelize.define('cuentas', {
        uuid: {
            type: type.UUID,
            allowNull: false,
            defaultValue: type.UUIDV1
        },
        id: {
            type: type.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        fecha_alta: {
            type: type.DATEONLY,
            allowNull: false,
            defaultValue: moment().format('YYYY-MM-DD')
        },
        idusuario: {
            type: type.STRING(100),
            allowNull: false
        },
        telefono: {
            type: type.STRING(10),
            allowNull: false,
            primaryKey: true,
            unique: true
        },
        email: {
            type: type.STRING(100),
            allowNull: false,
            unique: true
        },
        password: {
            type: type.STRING(150),
            allowNull: false
        },
        status: {
            type: type.INTEGER,
            allowNull: false,
            defaultValue: 1
        },
        user_token: {
            type: type.TEXT,
            allowNull: false,
            defaultValue: ''
        }
    }, 
    {
        timestamps: false
    });
};