const moment = require("moment");

module.exports = (sequelize, type) => {
    return sequelize.define('usuarios', {
        uuid: {
            type: type.UUID,
            allowNull: false,
            defaultValue: type.UUIDV1
        },
        id: {
            type: type.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        fecha_alta: {
            type: type.DATEONLY,
            allowNull: false,
            defaultValue: moment().format('YYYY-MM-DD')
        },
        idusuario: {
            type: type.STRING(100),
            allowNull: false
        },
        nombre: {
            type: type.STRING(100),
            allowNull: false,
            defaultValue: ''
            
        },
        ap_paterno: {
            type: type.STRING(100),
            allowNull: false
            
        },
        ap_materno: {
            type: type.STRING(15),
            allowNull: false
        },
        fecha_nac: {
            type: type.DATEONLY,
            allowNull: false
        },
    }, 
    {
        timestamps: false
    });
};