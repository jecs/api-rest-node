'user strict'

const jwt = require('jwt-simple');
const moment = require('moment');
const bcrypt = require('bcryptjs');
const config = require('./config')

function createToken(user) {
    const payload = {
        user:user,
        iat: moment().unix(),
        exp: moment().add(1, 'year').unix()
    }

    return jwt.encode(payload, config.SECRET_TOKEN)
}

function createSessionToken(user) {
    const payload = {
        user: user,
        iat: moment().unix(),
        exp: moment().add(2, 'hour').unix()
    }

    return jwt.encode(payload, config.SECRET_TOKEN)
}

function decodeSessionToken(token, mac) {
    const decode = new Promise((resolve, reject) => {
        try {
            const payload = jwt.decode(token, config.SECRET_TOKEN);
            if (payload.exp <= moment().unix()) {
                reject({
                    status: 401,
                    message: 'Unauthorized - Invalid Token'
                });
            } 

            resolve({status:200, message:"fine"});            
            
        } catch(err) {
            reject({
                status: 500,
                message: 'Internal Server Error - Invalid Token'
            });
        }
    });
    return decode;
}

module.exports = {
    createToken,
    createSessionToken,
    decodeSessionToken
}