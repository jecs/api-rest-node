module.exports = {
    port: process.env.PORT || 3000,
    database: process.env.DBNAME || 'database',
    dbUser: process.env.DBUSER || 'dbuser',
    dbPassword: process.env.DBPWD || 'dbpassword',
    dbHost: process.env.DBHOST || 'dbhost',
    dbDialect: process.env.DIALECT || 'postgres',
    SECRET_TOKEN: 'adfgtewawfeANSMRTEHRDFcaXCVAFSNABN34WEF'

}