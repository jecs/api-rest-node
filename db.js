'use strict';
const Sequelize = require('sequelize');
const config = require('./util/config');

const UsersModel = require('./models/users');
const AccountsModel = require('./models/accounts');

const sequelize = new Sequelize(config.database, config.dbUser, config.dbPassword,{
    host: config.dbHost,
    dialect: config.dbDialect
});

const Users = UsersModel(sequelize, Sequelize);
const Accounts = AccountsModel(sequelize, Sequelize);

sequelize.sync({force: true})
.then( () => {    
    console.log("Base de datos sincronizada");
});

module.exports = {
    Users,
    Accounts
};
