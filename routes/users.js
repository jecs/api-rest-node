var express = require('express');
var router = express.Router();

const userCtrl = require('../controllers/ctrlUsers');
const { check, validationResult } = require('express-validator');

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
 
router.get('/getall', function(req, res, next) {
  Promise.all([userCtrl.getAllUsers(req, res)]).then(values => {});
});

router.post('/register', [
  check('telefono','Parametros incorrectos.').not().isEmpty(),
  check('email','Parametros incorrectos.').isEmail(),
  check('password','Parametros incorrectos.').not().isEmpty(),
  check('nombre','Parametros incorrectos.').not().isEmpty(),
  check('paterno','Parametros incorrectos.').not().isEmpty(),
  check('materno','Parametros incorrectos.').not().isEmpty(),
  check('fechanac','Parametros incorrectos.').not().isEmpty()
],function(req, res, next) {
  Promise.all([userCtrl.register(req, res)]).then(values => {});
});

module.exports = router;